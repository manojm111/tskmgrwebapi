﻿using NUnit.Framework;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using WebAPICodeFirst.Controllers;
using WebAPICodeFirst.Models.Domain;

namespace UnitTestProject

{
    [TestFixture]
    public class UserTest
    {
        [Test]
        public void GetAllUsers()
        {
            // Set up Prerequisites  
            var controller = new UserController ();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            
            // Act on Test    
            int expectedUserCount =6;

            // Assert the result 
            var response = controller.Get();
            var contentResult = response as OkNegotiatedContentResult<User>;
            int actualUserCount = ((OkNegotiatedContentResult<System.Collections.Generic.List<User>>)response).Content.Count;

            Assert.AreEqual(expectedUserCount, actualUserCount);

        }

        [Test]
        public void GetUserByIdSuccess()
        {
            // Set up Prerequisites 
            var controller = new UserController();

            // Act on Test 
            int id = 4;
            var response = controller.Get(id);
            var contentResult = response as OkNegotiatedContentResult<User>;

            // Assert the result 
            Assert.IsNotNull(contentResult);          
            Assert.AreEqual(4, contentResult.Content.UserId);
        }

        [Test]
        public void AddNewUserTest()
        {
            // Arrange 
            var controller = new UserController();
            User newUser = new User
            {
                FirstName="Manoj",
                LastName="Madhavan",
                EmployeeId=310502
                
            };
            // Act 
            var actionResult = controller.Post(newUser);
            // Assert          
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);

        }

        [Test]
        public void UpdateUser()
        {
            // Arrange
            var controller = new UserController();
            // Act
            int userIdtoUpdate = 9;

            var usertoUpdate = new User
            {

                FirstName = "Carter 1",
                LastName = "Aiden 1",
                EmployeeId = 3105021

            };

            var actionResult = controller.UpdateUser(userIdtoUpdate, usertoUpdate);
            // Assert          
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);

        }

        [Test]
        public void DeleteUser()
        {
            // Arrange
            var controller = new UserController();
            int userIdToBeDeleted = 10;
            var actionResult = controller.Delete(userIdToBeDeleted);
            // Assert
            Assert.AreEqual(userIdToBeDeleted, ((OkNegotiatedContentResult<int>)actionResult).Content);
            

        }
    }

}

