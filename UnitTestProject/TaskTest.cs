﻿using NUnit.Framework;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using WebAPICodeFirst.Controllers;
using WebAPICodeFirst.Models.Domain;

namespace UnitTestProject

{
    [TestFixture]
    public class TaskTest
    {
        [Test]
        public void GetAllTasks()
        {
            // Set up Prerequisites  
            var controller = new TaskController ();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            
            // Act on Test    
            int expectedTaskCount =8;

            // Assert the result 
            var response = controller.Get();
            var contentResult = response as OkNegotiatedContentResult<Task>;
            int actualTaskCount = ((OkNegotiatedContentResult<System.Collections.Generic.List<Task>>)response).Content.Count;

            Assert.AreEqual(expectedTaskCount, actualTaskCount);

        }

        [Test]
        public void GetTaskByIdSuccess()
        {
            // Set up Prerequisites 
            var controller = new TaskController();

            // Act on Test 
            int id = 3;
            var response = controller.Get(id);
            var contentResult = response as OkNegotiatedContentResult<Task>;

            // Assert the result 
            Assert.IsNotNull(contentResult);          
            Assert.AreEqual(3, contentResult.Content.TaskId);
        }

        //[Test]
        //public void GetTaskNotFound()
        //{
        //    // Set up Prerequisites  
        //    var controller = new TaskController();
        //    // Act 
        //    IHttpActionResult actionResult = controller.Get(100);
        //    // Assert 
        //    Assert.That(actionResult.co, Is.TypeOf<NotFoundResult>());
        //}

        [Test]
        public void AddNewTaskTest()
        {
            // Arrange 
            var controller = new TaskController();
            Task newTask = new Task
            {
                TaskName="Busniess Review",
                Priority=8,
                ParentId=2002,
                StartDate= Convert.ToDateTime( "2019-10-01"),
                EndDate = Convert.ToDateTime ("2019-10-15")
            };
            // Act 
            var actionResult = controller.Post(newTask);
            // Assert          
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);

        }

        [Test]
        public void UpdateTask()
        {
            // Arrange
            var controller = new TaskController();
            // Act
            int taskIdtoUpdate = 10;

            var tasktoUpdate = new Task
            {
               
                TaskName = "Busniess Review3",
                Priority = 4,
                ParentId = 2002,
                StartDate = Convert.ToDateTime("2019-10-01"),
                EndDate = Convert.ToDateTime("2019-10-15")

            };

            var actionResult = controller.UpdateTask(taskIdtoUpdate, tasktoUpdate);
            // Assert          
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);

        }

        [Test]
        public void EndTask()
        {
            // Arrange
            var controller = new TaskController();
            int taskIdToBeEnded = 10;
            var actionResult = controller.EndTask(taskIdToBeEnded);
            // Assert
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);
            

        }
    }

}

