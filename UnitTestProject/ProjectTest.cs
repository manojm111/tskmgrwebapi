﻿using NUnit.Framework;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using WebAPICodeFirst.Controllers;
using WebAPICodeFirst.Models.Domain;

namespace UnitTestProject

{
    [TestFixture]
    public class ProjectTest
    {
        [Test]
        public void GetAllProjects()
        {
            // Set up Prerequisites  
            var controller = new ProjectController ();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            
            // Act on Test    
            int expectedProjectCount =8;

            // Assert the result 
            var response = controller.Get();
            var contentResult = response as OkNegotiatedContentResult<Project>;
            int actualProjectCount = ((OkNegotiatedContentResult<System.Collections.Generic.List<Project>>)response).Content.Count;

            Assert.AreEqual(expectedProjectCount, actualProjectCount);

        }

        [Test]
        public void GetProjectByIdSuccess()
        {
            // Set up Prerequisites 
            var controller = new ProjectController();

            // Act on Test 
            int id = 2;
            var response = controller.Get(id);
            var contentResult = response as OkNegotiatedContentResult<Project>;

            // Assert the result 
            Assert.IsNotNull(contentResult);          
            Assert.AreEqual(2, contentResult.Content.ProjectId);
        }

        [Test]
        public void AddNewProjectTest()
        {
            // Arrange 
            var controller = new ProjectController();
            Project newProject = new Project
            {
                ProjectName="PayPal Process",                
                Priority = 8,                
                StartDate = Convert.ToDateTime("2019-10-01"),
                EndDate = Convert.ToDateTime("2019-10-15")

            };
            // Act 
            var actionResult = controller.Post(newProject);
            // Assert          
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);

        }

        [Test]
        public void UpdateProject()
        {
            // Arrange
            var controller = new ProjectController();
            // Act
            int projectIdtoUpdate = 8;

            var projecttoUpdate = new Project
            {

                ProjectName = "project111",
                Priority = 9,
                StartDate = Convert.ToDateTime("2019-10-01"),
                EndDate = Convert.ToDateTime("2019-10-15"),
                

            };

            var actionResult = controller.UpdateProject(projectIdtoUpdate, projecttoUpdate);
            // Assert          
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);

        }

        [Test]
        public void SuspendProject()
        {
            // Arrange
            var controller = new ProjectController();
            int projectIdToBeSuspended = 9;
            var actionResult = controller.SuspendProject(projectIdToBeSuspended);
            // Assert
            Assert.AreEqual(1, ((OkNegotiatedContentResult<int>)actionResult).Content);
            

        }
    }

}

