namespace WebAPICodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingtasktable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Task",
                c => new
                    {
                        TaskId = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        TaskName = c.String(maxLength: 100),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.TaskId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Task");
        }
    }
}
