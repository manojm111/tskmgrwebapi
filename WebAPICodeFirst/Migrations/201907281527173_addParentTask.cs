namespace WebAPICodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addParentTask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ParentTask",
                c => new
                    {
                        ParentTaskId = c.Int(nullable: false, identity: true),
                        ParentTaskName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.ParentTaskId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ParentTask");
        }
    }
}
