namespace WebAPICodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatusinTaskTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Task", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Task", "Status");
        }
    }
}
