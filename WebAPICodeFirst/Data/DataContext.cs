﻿using System.Data.Entity;
using WebAPICodeFirst.Models.Domain;

namespace WebAPICodeFirst.Data
{    public class DataContext:DbContext
    {
        public DataContext():base("name=MyConString")
        {
        }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<ParentTask> ParentTasks { get; set; }
        public virtual DbSet<Project> Projects { get; set; }

        public virtual DbSet<User> Users { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .ToTable("Task")
                .HasKey(t => t.TaskId)
                .Property(t => t.TaskName).HasMaxLength(100);

            modelBuilder.Entity<ParentTask>()
              .ToTable("ParentTask")
              .HasKey(t => t.ParentTaskId)
              .Property(t => t.ParentTaskName).HasMaxLength(100);

            modelBuilder.Entity<Project>()
             .ToTable("Project")
             .HasKey(t => t.ProjectId)
             .Property(t => t.ProjectName).HasMaxLength(250);

            modelBuilder.Entity<User>()
           .ToTable("User")
           .HasKey(u => u.UserId)
           .Property(u => u.FirstName).HasMaxLength(50);


            modelBuilder.Entity<Task>()
                .HasRequired(t => t.ParentTask)
               .WithMany(t => t.Tasks)
               .HasForeignKey(t => t.ParentId);


            base.OnModelCreating(modelBuilder);
        }
    }
}