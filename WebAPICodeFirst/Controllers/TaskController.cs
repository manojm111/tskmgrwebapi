﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Http;
using WebAPICodeFirst.Data;
using WebAPICodeFirst.Models.Domain;

namespace WebAPICodeFirst.Controllers
{

    [RoutePrefix("api/task")]
    //[EnableCors(origins: "http://localhost:4200/",headers:"*",methods:"*")]
    public class TaskController : ApiController
    {
        private readonly DataContext _dataContext;

        public TaskController()
        {
            _dataContext = new DataContext();
        }
        public IHttpActionResult Post(Task task)
        {
            if (task.StartDate <= (DateTime)SqlDateTime.MinValue)
                task.StartDate = Convert.ToDateTime("01/01/1900");
            if (task.EndDate <= (DateTime)SqlDateTime.MinValue)
                task.EndDate = Convert.ToDateTime("01/01/1900");

            task.Status = true;
            _dataContext.Tasks.Add(task);
            var recordsInserted = _dataContext.SaveChanges();
            return Ok(recordsInserted);
        }
        public IHttpActionResult Get()
        {
            var data = _dataContext.Tasks.ToList();
            return Ok(data);
        }

        public IHttpActionResult Get(int id)
        {
            var data = _dataContext.Tasks.Find(id);
            return Ok(data);
        }

        [HttpGet]
        [Route("GetAllParentTask")]
        public IHttpActionResult GetAllParentTask()
        {
            var data = _dataContext.ParentTasks.Select(parentTask => new
            {
                parentTask.ParentTaskId,
                parentTask.ParentTaskName              
            });
            return Ok(data);
        }        

        [HttpPost]
        [Route("UpdateTask/{id:int}")]
        public IHttpActionResult UpdateTask(int id, [FromBody] Task task)
        {
            int dataUpdated = 0;
            var dataToUpdate = _dataContext.Tasks.Find(id);
            if (dataToUpdate != null)
            {
                dataToUpdate.ParentId = task.ParentId;
                dataToUpdate.TaskName = task.TaskName;
                dataToUpdate.StartDate = task.StartDate;
                dataToUpdate.EndDate = task.EndDate;
                dataToUpdate.Priority = task.Priority;            

                dataUpdated = _dataContext.SaveChanges();
            }
            return Ok(dataUpdated);
        }

        [HttpPost]
        [Route("EndTask/{id:int}")]
        public IHttpActionResult EndTask(int id)
        {       
            int dataUpdated = 0;
            var dataToUpdate = _dataContext.Tasks.Find(id);
            if (dataToUpdate != null)
            {

                dataToUpdate.Status = false;
                dataUpdated = _dataContext.SaveChanges();
            }
            return Ok(dataUpdated);
        }

        [HttpPost]
        [Route("TestPost/{id:int}")]
        public string TestPost(int id, [FromBody] string value)
        {
            return "hello" + value;
        }

        public IHttpActionResult Delete(int id)
        {
            var dataToDelete = _dataContext.Tasks.Find(id);
            if (dataToDelete != null)
            {
                _dataContext.Tasks.Remove(dataToDelete);
                _dataContext.SaveChanges();
            }
            return Ok(dataToDelete.TaskId);

        }

    }
}
