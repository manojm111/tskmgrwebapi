﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Http;
using WebAPICodeFirst.Data;
using WebAPICodeFirst.Models.Domain;

namespace WebAPICodeFirst.Controllers
{
    [RoutePrefix("api/project")]
    public class ProjectController : ApiController
    {
        private readonly DataContext _dataContext;

        public ProjectController ()
        {
            _dataContext = new DataContext();
        }

        public IHttpActionResult Post(Project project)
        {
            if (project.StartDate <= (DateTime)SqlDateTime.MinValue)
                project.StartDate = Convert.ToDateTime("01/01/1900");
            if (project.EndDate <= (DateTime)SqlDateTime.MinValue)
                project.EndDate = Convert.ToDateTime("01/01/1900");

            _dataContext.Projects.Add(project);         


            var recordsInserted = _dataContext.SaveChanges();
            return Ok(recordsInserted);
        }
        public IHttpActionResult Get()
        {
            var data = _dataContext.Projects.ToList();
            return Ok(data);
        }

        public IHttpActionResult Get(int id)
        {
            var data = _dataContext.Projects.Find(id);
            return Ok(data);
        }

        [HttpPost]
        [Route("UpdateProject/{id:int}")]
        public IHttpActionResult UpdateProject(int id, [FromBody] Project project)
        {
            int dataUpdated = 0;
            var dataToUpdate = _dataContext.Projects.Find(id);
            if (dataToUpdate != null)
            {
                
                dataToUpdate.ProjectName = project.ProjectName;
                dataToUpdate.StartDate = project.StartDate;
                dataToUpdate.EndDate = project.EndDate;
                dataToUpdate.Priority = project.Priority;

                dataUpdated = _dataContext.SaveChanges();
            }
            return Ok(dataUpdated);
        }

        [HttpPost]
        [Route("SuspendProject/{id:int}")]
        public IHttpActionResult SuspendProject(int id)
        {
            int dataUpdated = 0;
            var dataToUpdate = _dataContext.Projects.Find(id);
            if (dataToUpdate != null)
            {

                dataToUpdate.Status = false;
                dataUpdated = _dataContext.SaveChanges();
            }
            return Ok(dataUpdated);
        }

        public IHttpActionResult Delete(int id)
        {
            var dataToDelete = _dataContext.Projects.Find(id);
            if (dataToDelete != null)
            {
                _dataContext.Projects.Remove(dataToDelete);
                _dataContext.SaveChanges();
            }
            return Ok(dataToDelete.ProjectId);

        }
    }
}
