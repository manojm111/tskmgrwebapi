﻿using System.Linq;
using System.Web.Http;
using WebAPICodeFirst.Data;
using WebAPICodeFirst.Models.Domain;

namespace WebAPICodeFirst.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly DataContext _dataContext;

        public UserController ()
        {
            _dataContext = new DataContext();
        }

        public IHttpActionResult Post(User user)
        {
            _dataContext.Users.Add(user);
            var recordsInserted = _dataContext.SaveChanges();
            return Ok(recordsInserted);
        }
        public IHttpActionResult Get()
        {
            var data = _dataContext.Users.ToList();
            return Ok(data);
        }

        public IHttpActionResult Get(int id)
        {
            var data = _dataContext.Users.Find(id);
            return Ok(data);
        }

        [HttpPost]
        [Route("UpdateUser/{id:int}")]
        public IHttpActionResult UpdateUser(int id, [FromBody]   User   user)
        {
            int dataUpdated = 0;
            var dataToUpdate = _dataContext.Users.Find(id);
            if (dataToUpdate != null)
            {

                dataToUpdate.FirstName = user.FirstName;
                dataToUpdate.LastName = user.LastName;
                dataToUpdate.EmployeeId =   user.EmployeeId;
                //dataToUpdate.ProjectId =   user.ProjectId;
              //  dataToUpdate.TaskId =   user.TaskId;

                dataUpdated = _dataContext.SaveChanges();
            }
            return Ok(dataUpdated);
        }
      
        public IHttpActionResult Delete(int id)
        {
            var dataToDelete = _dataContext.Users.Find(id);
            if (dataToDelete != null)
            {
                _dataContext.Users.Remove(dataToDelete);
                _dataContext.SaveChanges();
            }
            return Ok(dataToDelete.UserId);

        }
    }
}
