﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPICodeFirst.Models.Domain
{
    public class ParentTask
    {
        public  ParentTask ()
          {
            Tasks = new HashSet<Task>();
          }

        public int ParentTaskId { get; set; }
        public string ParentTaskName { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
    }
}