﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPICodeFirst.Models.Domain
{
    public class Project
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }        
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Priority { get; set; }
        public int UserId { get; set; }
        public bool Status { get; set; }
    }
}