﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPICodeFirst.Models.Domain
{
    public class Task
    {
        public int  TaskId { get; set; }
        public int ParentId { get; set; }
        public string  TaskName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Priority { get; set; }
        public ParentTask ParentTask { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public bool Status { get; set; }
    }
}